# Rule Engine

A Java engine for rules based on boolean operations in a easy readable syntax

## Building

    mvn package
    
## Usage

### Boolean operations

A rule is an expression consisting of the basic boolean functions. Allowed are
the boolean constants `TRUE` and `FALSE` Given two boolean variables _a_ and _b_
you may use the following operators:

* `NOT a`
* `a AND b`
* `a OR b`

### Decimal values and relational operators

Rules also consist decimal constants without fractions (e.g. 42, but not 42.5).
Decimal values may be compared using relational operators. Given two decimal 
variables _n_ and _m_, the following relational are allowed:

* `a < b`
* `a <= b`
* `a >= b`
* `a > b`
* `a == b`
* `a != b`

### String values

String values must be quoted in single quotes, e.g. `"some string value"`. You may
compare them in a rule using `==` and `!=`.

### Parentheses

The evaluation fulfills the order of the boolean algebra (`NOT` binds stronger than `AND`,
`AND` binds stronger than `OR`). Sub expressions in parentheses will be evaluated first, e.g.

* `(a OR b) AND c`
* `(a AND (b OR c)) AND c`

### Rule variables

Rule variables may be added to the rule evaluator by creating a `RuleVariables` object and
put a variable name and value pair to it, e.g.

```java
RuleVariables ruleVariables = new RuleVariables();
ruleVariables.put("nameA", "value");
ruleVariables.put("nameB", true);
ruleVariables.put("nameC", 42);
```

These variables may be referenced in a rule by using their name, for example

```
nameA == "value" AND nameB AND nameC == 42
```

### Rule evaluation

To evaluate a rule, create a `RuleEvaluator object and call the method
`evaluate(String rule)` on it:

```java
RuleEvaluator ruleEvaluator = new RuleEvaluator();
ruleEvaluator.evaluate("42 > 0 AND true"); // will return true

```

You may pass a `RuleVariables` object to the constructor to reference variables:

```java
RuleVariables ruleVariables = new RuleVariables();
ruleVariables.putValue("someNumber", 42);
ruleVariables.putValue("someBoolean", true);
RuleEvaluator ruleEvaluator = new RuleEvaluator(ruleVariables);
ruleEvaluator.evaluate("someNumber > 0 AND someBoolean"); // will return true
```