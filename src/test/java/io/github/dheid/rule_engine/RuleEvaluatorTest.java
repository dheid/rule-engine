package io.github.dheid.rule_engine;

import io.github.dheid.rule_engine.exception.RuleSyntaxException;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by daniel on 04.07.17.
 */
public class RuleEvaluatorTest {

    private RuleVariables ruleVariables = new RuleVariables();

    private RuleEvaluator ruleEvaluator = new RuleEvaluator(ruleVariables);

    @Test
    public void evaluatesValidExpression() throws Exception {
        boolean actual = ruleEvaluator.evaluate("TRUE");
        assertThat(actual, is(true));
    }

    @Test(expected = RuleSyntaxException.class)
    public void throwsExceptionOnInvalidExpression() throws Exception {
        ruleEvaluator.evaluate("/&/&%(/%´´´");
    }

}