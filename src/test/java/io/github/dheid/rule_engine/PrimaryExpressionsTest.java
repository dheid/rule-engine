package io.github.dheid.rule_engine;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by daniel on 04.07.17.
 */
public class PrimaryExpressionsTest {

    @Test
    public void storesPrimaryExpression() throws Exception {

        PrimaryExpressions primaryExpressions = new PrimaryExpressions();
        primaryExpressions.push("something");

        assertThat(primaryExpressions.pop(), is("something"));

    }
}