package io.github.dheid.rule_engine;

import io.github.dheid.rule_engine.exception.MissingRuleVariableException;
import org.junit.Test;

/**
 * Created by daniel on 04.07.17.
 */
public class RuleVariablesTest {

    @Test(expected = MissingRuleVariableException.class)
    public void throwsExceptionOnUnknownVariable() throws Exception {
        RuleVariables ruleVariables = new RuleVariables();
        ruleVariables.getValue("nonExisting");
    }
}