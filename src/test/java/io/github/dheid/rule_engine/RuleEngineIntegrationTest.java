package io.github.dheid.rule_engine;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.junit.runners.Parameterized.*;

/**
 * Created by daniel on 01.07.17.
 */
@RunWith(Parameterized.class)
public class RuleEngineIntegrationTest {

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"         TRUE", true},
                {"         FALSE", false},
                {"         booleanVarA", true},
                {"         booleanVarA //some line comment", true},
                {"         booleanVarB", false},
                {"         booleanVarB /* some block level comment */", false},
                {"         booleanVarA AND     booleanVarA", true},
                {"         booleanVarA AND     booleanVarB", false},
                {"         booleanVarB AND     booleanVarA", false},
                {"         booleanVarB AND     booleanVarB", false},
                {"    NOT  booleanVarA AND NOT booleanVarA", false},
                {"    NOT  booleanVarA AND NOT booleanVarB", false},
                {"    NOT  booleanVarB AND NOT booleanVarA", false},
                {"    NOT  booleanVarB AND NOT booleanVarB", true},
                {"    NOT (booleanVarA AND     booleanVarA)", false},
                {"    NOT (booleanVarA AND     booleanVarB)", true},
                {"    NOT (booleanVarB AND     booleanVarA)", true},
                {"    NOT (booleanVarB AND     booleanVarB)", true},
                {"         booleanVarA OR      booleanVarA", true},
                {"         booleanVarA OR      booleanVarB", true},
                {"         booleanVarB OR      booleanVarA", true},
                {"         booleanVarB OR      booleanVarB", false},
                {"    NOT  booleanVarA OR NOT  booleanVarA", false},
                {"    NOT  booleanVarA OR NOT  booleanVarB", true},
                {"    NOT  booleanVarB OR NOT  booleanVarA", true},
                {"    NOT  booleanVarB OR NOT  booleanVarB", true},
                {"    NOT booleanVarB", true},
                {"    (booleanVarA OR booleanVarB) AND booleanVarA", true},
                {"    booleanVarA == booleanVarA", true},
                {"    booleanVarA != booleanVarA", false},
                {"    booleanVarA == booleanVarB", false},
                {"    booleanVarA != booleanVarB", true},
                {"    intVarA < 5", true},
                {"    intVarA <= 5", true},
                {"    intVarA >= 5", false},
                {"    intVarA > 5", false},
                {"    intVarB < 5", false},
                {"    intVarB <= 5", true},
                {"    intVarB >= 5", true},
                {"    intVarB > 5", false},
                {"    intVarC < 5", false},
                {"    intVarC <= 5", false},
                {"    intVarC >= 5", true},
                {"    intVarC > 5", true},
                {"    intVarA == intVarA", true},
                {"    intVarA == intVarB", false},
                {"    intVarA != intVarB", true},
                {"    intVarA == intVarB", false},
                {"    stringVarA == \"hello\"", true},
                {"    stringVarA != \"hello\"", false},
                {"    stringVarA == \"\"", false},
                {"    stringVarA != \"\"", true},
                {"    stringVarB == \"hello world\"", true},
                {"    stringVarB != \"hello world\"", false},
                {"    dateVarA == 2017-07-30", true},
                {"    dateVarA != 2017-07-30", false},
                {"    dateVarA == 2016-06-29", false},
                {"    dateVarA != 2016-06-29", true},
                {"    dateVarB == 2011-11-11", true},
                {"    dateVarB != 2011-11-11", false},
                {"    dateVarB == 2010-10-10", false},
                {"    dateVarB != 2010-10-10", true},
                {"    dateVarC == 2012-12-12", true},
                {"    dateVarC != 2012-12-12", false},
                {"    dateVarC == 2009-09-09", false},
                {"    dateVarC != 2009-09-09", true},
        });
    }

    private static RuleEvaluator ruleEvaluator;

    private final String rule;

    private final boolean expectedResult;

    @BeforeClass
    public static void initializeVariables() {
        RuleVariables ruleVariables = new RuleVariables();

        ruleVariables.putValue("booleanVarA", true);
        ruleVariables.putValue("booleanVarB", false);

        ruleVariables.putValue("intVarA", 4);
        ruleVariables.putValue("intVarB", 5);
        ruleVariables.putValue("intVarC", 6);

        ruleVariables.putValue("stringVarA", "hello");
        ruleVariables.putValue("stringVarB", "hello world");

        Calendar cal = Calendar.getInstance();
        cal.set(2017, Calendar.JULY, 30);
        ruleVariables.putValue("dateVarA", cal.getTime());
        ruleVariables.putValue("dateVarB", LocalDate.of(2011, 11, 11));
        ruleVariables.putValue("dateVarC", LocalDateTime.of(2012, 12, 12, 1,2,3));

        ruleEvaluator = new RuleEvaluator(ruleVariables);
    }

    public RuleEngineIntegrationTest(String rule, boolean expectedResult) {
        this.rule = rule;
        this.expectedResult = expectedResult;
    }

    @Test
    public void testRule() throws Exception {
        assertThat(rule, ruleEvaluator.evaluate(rule), is(expectedResult));
    }
}