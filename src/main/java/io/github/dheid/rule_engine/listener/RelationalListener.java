package io.github.dheid.rule_engine.listener;

import io.github.dheid.rule_engine.PrimaryExpressions;
import io.github.dheid.rule_engine.RuleBaseListener;
import io.github.dheid.rule_engine.RuleParser;

import static java.util.Objects.requireNonNull;

/**
 * Created by daniel on 03.07.17.
 */
public class RelationalListener extends RuleBaseListener {

    private final PrimaryExpressions primaryExpressions;

    public RelationalListener(PrimaryExpressions primaryExpressions) {
        requireNonNull(primaryExpressions, "Primary expressions must not be null");
        this.primaryExpressions = primaryExpressions;
    }

    @Override
    public void exitRelationalExpression(RuleParser.RelationalExpressionContext ctx) {
        RuleParser.RelationalExpressionContext relationalExpression =
                ctx.relationalExpression();
        if (relationalExpression != null) {
            Integer secondOperand = (Integer) primaryExpressions.pop();
            Integer firstOperand = (Integer) primaryExpressions.pop();
            String operator = ctx.getChild(1).getText();
            Boolean primaryExpression = null;
            switch (operator) {
                case "<":
                    primaryExpression = firstOperand < secondOperand;
                    break;
                case ">":
                    primaryExpression = firstOperand > secondOperand;
                    break;
                case "<=":
                    primaryExpression = firstOperand <= secondOperand;
                    break;
                case ">=":
                    primaryExpression = firstOperand >= secondOperand;
                    break;
            }
            primaryExpressions.push(primaryExpression);
        }
    }

}
