package io.github.dheid.rule_engine.listener;

import io.github.dheid.rule_engine.PrimaryExpressions;
import io.github.dheid.rule_engine.RuleBaseListener;
import io.github.dheid.rule_engine.RuleParser;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.function.Function;

import static java.util.Objects.requireNonNull;

/**
 * Created by daniel on 03.07.17.
 */
public class NotListener extends RuleBaseListener {

    private final PrimaryExpressions primaryExpressions;

    public NotListener(PrimaryExpressions primaryExpressions) {
        requireNonNull(primaryExpressions, "Primary expressions must not be null");
        this.primaryExpressions = primaryExpressions;
    }

    @Override
    public void exitNotExpression(RuleParser.NotExpressionContext ctx) {
        if (ctx.notExpression() != null) {
            Boolean primaryExpression = (Boolean) primaryExpressions.pop();
            primaryExpressions.push(!primaryExpression);
        }
    }

}
