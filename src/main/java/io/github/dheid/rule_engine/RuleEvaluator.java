package io.github.dheid.rule_engine;

import io.github.dheid.rule_engine.listener.EqualityListener;
import io.github.dheid.rule_engine.listener.LogicalAndListener;
import io.github.dheid.rule_engine.listener.LogicalOrListener;
import io.github.dheid.rule_engine.listener.NotListener;
import io.github.dheid.rule_engine.listener.PrimaryListener;
import io.github.dheid.rule_engine.listener.RelationalListener;
import io.github.dheid.rule_engine.listener.RuleErrorListener;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * Created by daniel on 03.07.17.
 */
public class RuleEvaluator {

    public final RuleVariables ruleVariables;

    public RuleEvaluator(RuleVariables ruleVariables) {
        requireNonNull(ruleVariables, "Rule variables must not be null");
        this.ruleVariables = ruleVariables;
    }

    public RuleEvaluator(Map<String, Object> ruleVariables) {
        requireNonNull(ruleVariables, "Rule variables must not be null");
        this.ruleVariables = new RuleVariables(ruleVariables);
    }

    public RuleEvaluator() {
        this.ruleVariables = new RuleVariables();
    }

    public boolean evaluate(String rule) {

        requireNonNull(rule, "Rule must not be null");
        RuleErrorListener ruleErrorListener = new RuleErrorListener(rule);

        RuleLexer lexer = new RuleLexer(CharStreams.fromString(rule));
        lexer.removeErrorListeners();
        lexer.addErrorListener(ruleErrorListener);

        CommonTokenStream tokens = new CommonTokenStream(lexer);

        PrimaryExpressions primaryExpressions = new PrimaryExpressions();

        RuleParser parser = new RuleParser(tokens);

        parser.addParseListener(new PrimaryListener(primaryExpressions, ruleVariables));
        parser.addParseListener(new NotListener(primaryExpressions));
        parser.addParseListener(new RelationalListener(primaryExpressions));
        parser.addParseListener(new EqualityListener(primaryExpressions));
        parser.addParseListener(new LogicalAndListener(primaryExpressions));
        parser.addParseListener(new LogicalOrListener(primaryExpressions));

        parser.removeErrorListeners();
        parser.addErrorListener(ruleErrorListener);

        parser.expression();

        return (boolean) primaryExpressions.pop();
    }

}
